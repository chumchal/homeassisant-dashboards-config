0. Example Screenshot 
<img src="https://gitlab.com/chumchal/homeassisant-dashboards-config/-/raw/main/images/ecrot_dashboard.png"  width="300" height="600">

Require HACS Installations

1. Lovelace:
- [Mini Graph Card](https://github.com/kalkih/mini-graph-card/tree/master)

2. Integrations
- [HA Multiscrape](https://github.com/danieldotnl/ha-multiscrape)
